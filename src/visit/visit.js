export default class Visit {
    constructor(target, description, urgency, fullName, doctor, comments) {
        this.target = target
        this.description = description
        this.urgency = urgency
        this.fullName = fullName
        this.comments = comments
        this.doctor = doctor
    }
}