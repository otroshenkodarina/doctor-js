import Visit from "./visit.js";
import * as Function from '../constans/index.js'

export class VisitCardiologist extends Visit {
    constructor(
        target,
        description,
        urgency,
        fullName,
        doctor,
        comments,
        normalPressure,
        bodyMassIndex,
        diseases,
        age) {
        super(target, description, urgency, fullName, doctor, comments);
        this.target = target
        this.description = description
        this.urgency = urgency
        this.fullName = fullName
        this.doctor = doctor
        this.age = age
        this.bodyMassIndex = bodyMassIndex
        this.diseases = diseases
        this.normalPressure = normalPressure
        this.comments = comments
    }

    showCardiologist(parent) {
        parent.insertAdjacentHTML('afterend', `
        ${Function.makeCardiologistBlock(this.urgency, this.target, this.description, this.diseases, this.bodyMassIndex, this.normalPressure, this.age, this.comments)}      
         `)
    }
}
