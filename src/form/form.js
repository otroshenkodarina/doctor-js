import API from "../API/API.js";
import {Card} from "../Card/Card.js";
import {VisitTherapist} from '../visit/visitTherapist.js'
import {VisitCardiologist} from '../visit/visitCardiologist.js'
import {VisitDentist} from '../visit/visitDentist'
import {Filters} from "../filters/filter.js";


export default class Form {

    constructor() {
        this.ELEMENTS = {
            wrapper: document.createElement('div'),
            visitWrapper: document.createElement('form'),
            selectWrapper: document.createElement('div'),
            email: document.createElement('input'),
            password: document.createElement('input'),
            btn: document.createElement('button'),
            errorContainer: document.createElement('div'),
            error: document.createElement('p'),
            fullName: document.createElement('input'),
            purpose: document.createElement('input'),
            description: document.createElement('input'),


            formSelect: document.createElement('select'),
            formSelectOption1: document.createElement('option'),
            formSelectOption2: document.createElement('option'),
            formSelectOption3: document.createElement('option'),
            formSelectOption4: document.createElement('option'),


            doctorSelect: document.createElement('select'),
            doctorSelectOption1: document.createElement('option'),
            doctorSelectOption2: document.createElement('option'),
            doctorSelectOption3: document.createElement('option'),
            doctorSelectOption4: document.createElement('option'),


            pressure: document.createElement('input'),
            index: document.createElement('input'),
            diseases: document.createElement('input'),
            age: document.createElement('input'),
            lastDate: document.createElement('input'),
            dentistParent: document.createElement('div'),
            therapistParent: document.createElement('div'),
            cardiologistParent: document.createElement('div'),

            createButton: document.createElement('button'),

            comments: document.createElement('input'),

            editButton: document.createElement('button'),
        }
    }


    render() {
        const {wrapper, email, password, btn, errorContainer, error} = this.ELEMENTS

        wrapper.classList.add('my-wrapper')
        btn.classList.add('my-btn')
        btn.classList.add('my-submit')
        btn.innerText = 'LOG IN'
        error.classList.add('error-text')
        error.innerText = ''
        email.placeholder = 'Enter your email'
        email.classList.add('my-input')
        email.value = ''
        password.classList.add('my-input')
        password.placeholder = 'Enter your password'
        password.value = ''
        password.type = 'password'
        wrapper.append(email, password, errorContainer, btn)

        return wrapper;
    }


    handleClick(modalID) {
        const {email, password, error, btn, errorContainer} = this.ELEMENTS

        btn.addEventListener('click', (eve) => {
            eve.preventDefault()
            error.classList.add('error-text')

            API.auth({email: `${email.value}`, password: `${password.value}`})
                .then(token => {
                    localStorage.setItem('userToken', token)
                    error.remove()

                    document.querySelector('#add-btn').classList.add('active-btn')
                    document.querySelector('#img').style.display = 'none'

                    document.querySelector('#log-in').classList.remove('active-btn')
                    document.querySelector('#log-out').classList.add('active-btn')

                    this.checkCards()
                    document.querySelector(`#${modalID} .btn-close`).click()
                    document.querySelector('.filters').style.display = 'flex'
                    const filter = new Filters()
                })
                .catch(e => {
                    error.innerText = `You entered wrong email or password... 
                Try again`
                    errorContainer.append(error)
                    localStorage.removeItem('userToken')
                    console.error(e)
                })
        })
    }

    async checkCards() {
        const cards = await API.getAllCards()
        if (cards.length > 0) {
            cards.forEach(card => {
                if (card.doctor === 'Cardiologist') {
                    const oldCardio = new VisitCardiologist(card.target, card.description, card.urgency, card.fullName, card.doctor, card.comments, card.pressure, card.bodyMassIndex, card.diseases, card.age)
                    const newCard = new Card(card, Card.prototype.madeColor(card.urgency), oldCardio)
                    newCard.render(document.body.querySelector('.card-list'))
                } else if (card.doctor === 'Therapist') {
                    const oldTherapi = new VisitTherapist(card.target, card.description, card.urgency, card.fullName, card.doctor, card.age, card.comments)
                    const newCard = new Card(card, Card.prototype.madeColor(card.urgency), oldTherapi)
                    newCard.render(document.body.querySelector('.card-list'))
                } else {
                    const oldDanti = new VisitDentist(card.target, card.description, card.urgency, card.fullName, card.doctor, card.lastDate, card.comments)
                    const newCard = new Card(card, Card.prototype.madeColor(card.urgency), oldDanti)
                    newCard.render(document.body.querySelector('.card-list'))
                }
            })
        } else {
            document.querySelector('#msg').classList.add('active-msg')
        }

    }

    visit(parent) {
        const {
            fullName,
            description,
            purpose,
            formSelect,
            formSelectOption1,
            formSelectOption2,
            formSelectOption3,
            formSelectOption4,
            visitWrapper,
            doctorSelect,
            doctorSelectOption1,
            doctorSelectOption2,
            doctorSelectOption3,
            doctorSelectOption4,
            selectWrapper,
            createButton,
            pressure,
            index,
            diseases,
            age,
            comments,
            errorContainer,
        } = this.ELEMENTS
        fullName.placeholder = 'Full Name'
        fullName.value = ''
        fullName.classList.add('visit-input')
        fullName.maxLength = 35

        pressure.value = ''
        diseases.value = ''
        index.value = ''
        age.value = ''

        description.placeholder = 'description'
        description.value = ''
        description.classList.add('visit-input')
        description.maxLength = 100

        purpose.placeholder = 'purpose of the visit'
        purpose.value = ''
        purpose.classList.add('visit-input')
        purpose.maxLength = 50

        formSelect.classList.add('visit-input')
        formSelect.classList.add('my-select')
        formSelectOption1.innerText = 'Urgency'
        formSelectOption1.setAttribute('disabled', 'disabled')
        formSelectOption1.setAttribute('selected', 'selected')

        formSelectOption2.innerText = 'Low'
        formSelectOption3.innerText = 'Normal'
        formSelectOption4.innerText = 'High'

        formSelect.append(formSelectOption1, formSelectOption2, formSelectOption3, formSelectOption4)

        doctorSelect.classList.add('visit-input')
        doctorSelect.classList.add('my-select')
        doctorSelectOption1.innerText = 'Doctors'
        doctorSelectOption1.setAttribute('disabled', 'disabled')
        doctorSelectOption1.setAttribute('selected', 'selected')

        doctorSelectOption2.innerText = 'Cardiologist'
        doctorSelectOption3.innerText = 'Dentist'
        doctorSelectOption4.innerText = 'Therapist'

        doctorSelect.append(doctorSelectOption1, doctorSelectOption2, doctorSelectOption3, doctorSelectOption4)

        comments.placeholder = 'Add your comments'
        comments.classList.add('visit-input')
        comments.value = ''
        comments.maxLength = 100

        createButton.innerText = 'CREATE CARD'
        createButton.classList.add('my-btn')
        createButton.classList.add('create-btn')

        visitWrapper.classList.add('visit-wrapper')
        visitWrapper.append(fullName, description, purpose, formSelect, doctorSelect, selectWrapper, comments, errorContainer, createButton)
        parent.append(visitWrapper)
        this.showDoctor()
        this.createCard(createButton, 'card-modal')
    }

    createCard(button, modalID) {
        const {
            doctorSelect,
            purpose,
            description,
            formSelect,
            fullName,
            pressure,
            index,
            diseases,
            age,
            lastDate,
            errorContainer,
            error,
            comments
        } = this.ELEMENTS

        button.addEventListener('click', (e) => {
            e.preventDefault()
            const filter = new Filters()

            if (doctorSelect.selectedIndex === 1 && fullName.value !== '' && description.value !== '' && pressure.value !== '' && diseases.value !== '' && index.value !== '' && age.value !== '') {
                error.remove()
                const cardiologistObj = new VisitCardiologist(purpose.value, description.value, formSelect.value, fullName.value, doctorSelect.value, comments.value, pressure.value, index.value, diseases.value, age.value)
                document.querySelector(`#${modalID} .btn-close`).click()
                API.addCard(cardiologistObj).then(data => {
                    const card = new Card(data, Card.prototype.madeColor(formSelect.value), cardiologistObj)
                    card.render(document.body.querySelector('.card-list'))
                })

            } else if (doctorSelect.selectedIndex === 2 && fullName.value !== '' && description.value !== '' && purpose.value !== '' && formSelect.value !== 'Urgency' && doctorSelect.value !== 'Doctors' && lastDate.value !== '') {
                error.remove()
                const dentistObj = new VisitDentist(purpose.value, description.value, formSelect.value, fullName.value, doctorSelect.value, lastDate.value, comments.value)
                document.querySelector(`#${modalID} .btn-close`).click()
                API.addCard(dentistObj).then(data => {
                    const card = new Card(data, Card.prototype.madeColor(formSelect.value), dentistObj)
                    card.render(document.body.querySelector('.card-list'))
                })

            } else if (doctorSelect.selectedIndex === 3 && fullName.value !== '' && description.value !== '' && purpose.value !== '' && formSelect.value !== 'Urgency' && doctorSelect.value !== 'Doctors' && age.value !== '') {
                error.remove()
                const therapistObj = new VisitTherapist(purpose.value, description.value, formSelect.value, fullName.value, doctorSelect.value, age.value, comments.value)
                document.querySelector(`#${modalID} .btn-close`).click()
                API.addCard(therapistObj).then(data => {
                    const card = new Card(data, Card.prototype.madeColor(formSelect.value), therapistObj)
                    card.render(document.body.querySelector('.card-list'))
                })

            } else {
                error.classList.add('error-text')
                error.innerText = 'Fill in all the fields'
                errorContainer.append(error)
            }
        })

    }

    cardiologist(parent) {
        const {pressure, diseases, index, age, cardiologistParent} = this.ELEMENTS
        pressure.placeholder = 'pressure'
        pressure.classList.add('visit-input')

        diseases.placeholder = 'diseases of the cardiovascular system'
        diseases.classList.add('visit-input')

        index.placeholder = 'index'
        index.classList.add('visit-input')

        age.placeholder = 'age'
        age.classList.add('visit-input')

        cardiologistParent.classList.add('visit-wrapper')
        cardiologistParent.append(pressure, diseases, index, age)
        parent.append(cardiologistParent)
    }

    dentist(parent) {
        const {lastDate, dentistParent} = this.ELEMENTS
        lastDate.placeholder = 'date of the last visit'
        lastDate.classList.add('visit-input')
        lastDate.type = 'date'


        dentistParent.append(lastDate)
        parent.append(dentistParent)

    }

    therapist(parent) {
        const {age, therapistParent} = this.ELEMENTS
        age.placeholder = 'age'
        age.classList.add('visit-input')
        therapistParent.append(age)
        parent.append(therapistParent)
    }

    showDoctor() {
        const {
            doctorSelect,
            dentistParent,
            therapistParent,
            cardiologistParent,
            selectWrapper,
        } = this.ELEMENTS
        doctorSelect.addEventListener('change', () => {
            if (doctorSelect.selectedIndex === 2) {
                therapistParent.remove()
                cardiologistParent.remove()
                this.dentist(selectWrapper)

            } else if (doctorSelect.selectedIndex === 1) {
                dentistParent.remove()
                therapistParent.remove()
                this.cardiologist(selectWrapper)

            } else if (doctorSelect.selectedIndex === 3) {
                dentistParent.remove()
                cardiologistParent.remove()

                this.therapist(selectWrapper)
            }
        })
    }

    async editCard(cardID) {
        const {
            createButton,
            editButton,
            visitWrapper,
            fullName,
            description,
            purpose,
            formSelect,
            doctorSelect,
            comments,
            pressure,
            diseases,
            index,
            age,
            lastDate,
            selectWrapper,
            error, errorContainer
        } = this.ELEMENTS

        createButton.remove()
        editButton.innerText = 'EDIT CARD'
        editButton.classList.add('my-btn')
        editButton.classList.add('edit-btn')

        visitWrapper.append(editButton)

        const oldCard = await API.getSingleCard(cardID)

        fullName.value = oldCard.fullName
        description.value = oldCard.description
        purpose.value = oldCard.target
        formSelect.value = oldCard.urgency
        doctorSelect.value = oldCard.doctor
        comments.value = oldCard.comments

        if (doctorSelect.value === 'Cardiologist') {
            pressure.value = oldCard.normalPressure
            diseases.value = oldCard.diseases
            index.value = oldCard.bodyMassIndex
            age.value = oldCard.age

            this.cardiologist(selectWrapper)

        } else if (doctorSelect.value === 'Dentist') {
            lastDate.value = oldCard.lastVisit

            this.dentist(selectWrapper)

        } else if (doctorSelect.value === 'Therapist') {
            age.value = oldCard.age

            this.therapist(selectWrapper)

        }

        document.querySelector('#card-modal').querySelector('.btn-close').addEventListener('click', () => {
            document.querySelector('.modal').style.display = 'none'
            document.querySelector('#card-modal').classList.remove('show')
        })

        editButton.addEventListener('click', (e) => {
            e.preventDefault()
            const filter = new Filters()

            if (doctorSelect.value === 'Therapist' && fullName.value !== '' && description.value !== '' && purpose.value !== '' && formSelect.value !== 'Urgency' && doctorSelect.value !== 'Doctors' && age.value !== '') {
                error.remove()
                const newCard = new VisitTherapist(purpose.value, description.value, formSelect.value, fullName.value, doctorSelect.value, age.value, comments.value)
                newCard.id = cardID

                API.editCard(newCard).then(data => {
                    document.getElementById(`${cardID}`).remove()
                    const card = new Card(data, Card.prototype.madeColor(formSelect.value), newCard)
                    card.render(document.body.querySelector('.card-list'))
                    let parent = document.getElementById(`${cardID}`)
                    parent.querySelector('.card-header').insertAdjacentHTML('beforeend', `<span class="grey">  [edited]</span>`)
                })

                document.querySelector('.modal').style.display = 'none'
                document.querySelector('#card-modal').classList.remove('show')
                filter.find()


            } else if (doctorSelect.value === 'Dentist' && fullName.value !== '' && description.value !== '' && purpose.value !== '' && formSelect.value !== 'Urgency' && doctorSelect.value !== 'Doctors' && lastDate.value !== '') {

                error.remove()

                const newCard = new VisitDentist(purpose.value, description.value, formSelect.value, fullName.value, doctorSelect.value, lastDate.value, comments.value)
                newCard.id = cardID

                API.editCard(newCard).then(data => {
                    document.getElementById(`${cardID}`).remove()
                    const card = new Card(data, Card.prototype.madeColor(formSelect.value), newCard)
                    card.render(document.body.querySelector('.card-list'))
                    let parent = document.getElementById(`${cardID}`)
                    parent.querySelector('.card-header').insertAdjacentHTML('beforeend', `<span class="grey">[edited]</span>`)
                })

                document.querySelector('.modal').style.display = 'none'
                document.querySelector('#card-modal').classList.remove('show')
                filter.find()


            } else if (doctorSelect.value === 'Cardiologist' && fullName.value !== '' && description.value !== '' && purpose.value !== '' && formSelect.value !== 'Urgency' && doctorSelect.value !== 'Doctors' && pressure.value !== '' && diseases.value !== '' && index.value !== '' && age.value !== '') {

                error.remove()

                const newCard = new VisitCardiologist(purpose.value, description.value, formSelect.value, fullName.value, doctorSelect.value, comments.value, pressure.value, index.value, diseases.value, age.value)
                newCard.id = cardID
                API.editCard(newCard).then(data => {
                    document.getElementById(`${cardID}`).remove()
                    const card = new Card(data, Card.prototype.madeColor(formSelect.value), newCard)
                    card.render(document.body.querySelector('.card-list'))
                    let parent = document.getElementById(`${cardID}`)
                    parent.querySelector('.card-header').insertAdjacentHTML('beforeend', `<span class="grey">[edited]</span>`)
                })

                document.querySelector('.modal').style.display = 'none'
                document.querySelector('#card-modal').classList.remove('show')
                filter.find()

            } else {
                error.classList.add('error-text')
                error.innerText = 'Fill in all the fields'
                errorContainer.append(error)
            }
        })

    }
}

